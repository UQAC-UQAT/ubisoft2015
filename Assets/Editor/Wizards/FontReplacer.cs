﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;

public class FontReplacer : ScriptableWizard
{

    public GameObject parentObject;
    public Font font;

    [MenuItem("CustomTools/Font replacer")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<FontReplacer>("Replace", "Replace");
    }

    void OnWizardCreate()
    {
        var texts = parentObject.GetComponentsInChildren<Text>();
        foreach (var text in texts)
        {
            text.font = font;
        }
    }

    void OnWizardUpdate()
    {
        helpString = "Replace shitload of fonts in UIText";
    }
}