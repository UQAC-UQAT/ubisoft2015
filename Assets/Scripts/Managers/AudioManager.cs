﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance { get; private set; }

	//Music
	public AudioClip ambiance;
	public AudioClip battle;

	//Effects
	public AudioClip menuConfirmNoise;
	public AudioClip menuSelectNoise;

	public AudioClip collision;
	public AudioClip repair;

	//Voices
	public AudioClip startGame;

	public AudioClip lateralPropellersActivated;
	public AudioClip verticalPropellersActivated;
	public AudioClip torpedoesActivated;
	public AudioClip repairActivated;
	public AudioClip lightActivated;
	public AudioClip flareActivated;

	public AudioClip propellersUsed;
	public AudioClip torpedoesUsed;
	public AudioClip repairUsed;
	public AudioClip lightUsed;
	public AudioClip flareUsed;

	//Voices can play
	private Hashtable canPlay = new Hashtable();
	private string lastPlayed;


	//Channels
	private AudioSource ambianceChannel;
	private AudioSource battleChannel;
	private AudioSource voiceChannel;
	private AudioSource repairChannel;
	
    void Awake()
    {
		if (instance == null)
		{
			instance = this;
		}
		
		else
			Destroy(gameObject);

		//Can play hashtable initialisation
		canPlay["startGame"] = true;
		canPlay["lateralPropellersActivated"] = true;
		canPlay["verticalPropellersActivated"] = true;
		canPlay["torpedoesActivated"] = true;
		canPlay["repairActivated"] = true;
		canPlay["lightActivated"] = true;
		canPlay["flareActivated"] = true;
		canPlay["propellersUsed"] = true;
		canPlay["torpedoesUsed"] = true;
		canPlay["repairUsed"] = true;
		canPlay["lightUsed"] = true;
		canPlay["flareUsed"] = true;

		ambianceChannel = gameObject.AddComponent<AudioSource>();
		ambianceChannel.clip = ambiance;
		ambianceChannel.loop = true;
		if(Application.loadedLevelName != "Menu"){
			battleChannel = gameObject.AddComponent<AudioSource>();
			battleChannel.clip = battle;
			battleChannel.loop = true;
		}

		ambianceChannel.Play ();
		ambianceChannel.volume = 1.0f;
		if(Application.loadedLevelName != "Menu"){
			battleChannel.Play ();
			battleChannel.volume = 0.0f;
		}

		voiceChannel = gameObject.AddComponent<AudioSource>();
		voiceChannel.volume = 0.2f;
		voiceChannel.loop = false;

		repairChannel = gameObject.AddComponent<AudioSource>();
		repairChannel.volume = 0.15f;
		repairChannel.loop = false;
    }


	//Battle
	public void StartBattle(){
		StartCoroutine("BattleThemeVolumeUp");
	}
	public void StopBattle(){
		StartCoroutine("BattleThemeVolumeDown");
	}

	private IEnumerator BattleThemeVolumeUp(){
		while(battleChannel.volume < 1.0f){
			battleChannel.volume += 0.01f;
			yield return new WaitForSeconds(0.1f);
		}
	}

	private IEnumerator BattleThemeVolumeDown(){
		while(battleChannel.volume > 0.0f){
			battleChannel.volume -= 0.01f;
			yield return new WaitForSeconds(0.1f);
		}
	}

	public void StopMusic(){
		ambianceChannel.Stop ();
		battleChannel.Stop ();
	}

	public void StartMusic(){
		ambianceChannel.Play ();
		battleChannel.Play ();
	}


	//Voices
	private IEnumerator NextSoundTimer(string key){
		canPlay[key] = false;
		yield return new WaitForSeconds(3.0f);
		canPlay[key] = true;
	}

	public void playIntro(){
		if((bool)canPlay["startGame"]){
			voiceChannel.PlayOneShot(startGame);
			StartCoroutine("NextSoundTimer", "startGame");
		}
	}

	public void playLateralPropellersSelected(){
		if((bool)canPlay["lateralPropellersActivated"] && lastPlayed != "lateralPropellersActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(lateralPropellersActivated);
			//StartCoroutine("NextSoundTimer", "lateralPropellersActivated");
			lastPlayed = "lateralPropellersActivated";
		}
	}

	public void playVerticalPropellersSelected(){
		if((bool)canPlay["verticalPropellersActivated"] && lastPlayed != "verticalPropellersActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(verticalPropellersActivated);
			//StartCoroutine("NextSoundTimer", "verticalPropellersActivated");
			lastPlayed = "verticalPropellersActivated";
		}
	}

	public void playTorpedoesActivated(){
		if((bool)canPlay["torpedoesActivated"] && lastPlayed != "torpedoesActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(torpedoesActivated);
			//StartCoroutine("NextSoundTimer", "torpedoesActivated");
			lastPlayed = "torpedoesActivated";
		}
	}

	public void playRepairActivated(){
		if((bool)canPlay["repairActivated"] && lastPlayed != "repairActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(repairActivated);
			//StartCoroutine("NextSoundTimer", "repairActivated");
			lastPlayed = "repairActivated";
		}
	}

	public void playLightActivated(){
		if((bool)canPlay["lightActivated"] && lastPlayed != "lightActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(lightActivated);
			//StartCoroutine("NextSoundTimer", "lightActivated");
			lastPlayed = "lightActivated";
		}
	}

	public void playFlareActivated(){
		if((bool)canPlay["flareActivated"] && lastPlayed != "flareActivated"){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(flareActivated);
			//StartCoroutine("NextSoundTimer", "flareActivated");
			lastPlayed = "flareActivated";
		}
	}

	public void playPropellersUsed(){
		if((bool)canPlay["propellersUsed"]/* && lastPlayed != "propellersUsed"*/){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(propellersUsed);
			StartCoroutine("NextSoundTimer", "propellersUsed");
			lastPlayed = "propellersUsed";
		}
	}

	public void playTorpedoesUsed(){
		if((bool)canPlay["torpedoesUsed"]/* && lastPlayed != "torpedoesUsed"*/){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(torpedoesUsed);
			StartCoroutine("NextSoundTimer", "torpedoesUsed");
			lastPlayed = "torpedoesUsed";
		}
	}

	public void playRepairUsed(){
		if((bool)canPlay["repairUsed"]/* && lastPlayed != "repairUsed"*/){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(repairUsed);
			StartCoroutine("NextSoundTimer", "repairUsed");
			lastPlayed = "repairUsed";
		}
	}

	public void playLightUsed(){
		if((bool)canPlay["lightUsed"]/* && lastPlayed != "lightUsed"*/){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(lightUsed);
			StartCoroutine("NextSoundTimer", "lightUsed");
			lastPlayed = "lightUsed";
		}
	}

	public void playFlareUsed(){
		if((bool)canPlay["flareUsed"]/* && lastPlayed != "flareUsed"*/){
			voiceChannel.Stop();
			voiceChannel.PlayOneShot(flareUsed);
			StartCoroutine("NextSoundTimer", "flareUsed");
			lastPlayed = "flareUsed";
		}
	}

	//effects
	public void playMenuSelect(){
		voiceChannel.PlayOneShot(menuSelectNoise);
	}

	public void playMenuConfirm(){
		voiceChannel.PlayOneShot(menuConfirmNoise);
	}

	public void playCollsion(){
		voiceChannel.PlayOneShot(collision);
	}

	public void playRepair(){
		repairChannel.PlayOneShot(repair);
	}
}
