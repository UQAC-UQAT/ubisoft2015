﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public struct Score
{
    public Score(string partnerName, int points, float time)
    {
        this.partnerName = partnerName;
        this.points = points;
        this.time = time;
    }

    public string partnerName;
    public int points;
    public float time;
}

public class LeaderboardManager {

    public static List<Score> scores { get; set; }

    private static int CompareScores(Score x, Score y)
    {
        if (x.points < y.points)
            return 1;
        else if (x.points > y.points)
            return -1;
        else
        {
            if (x.time > y.time)
                return 1;
            else if (x.time < y.time)
                return -1;
        }
        return 0;
    }

    public static bool AddScore(Score newScore)
    {
        if (scores.Count == 0)
        {
            scores.Add(newScore);
            SaveScores();
            return true;
        }

        if (CompareScores(newScore, scores[0]) < 0)
        {
            scores.Insert(0, newScore);
            SaveScores();
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void SaveScores()
    {
        string data;

        using (MemoryStream stream = new MemoryStream())
        {
            new BinaryFormatter().Serialize(stream, scores);
            data = Convert.ToBase64String(stream.ToArray());
        }

        PlayerPrefs.SetString("leaderboard", data);
    }

    public static void LoadScores()
    {
        string data = PlayerPrefs.GetString("leaderboard");
        if (string.IsNullOrEmpty(data))
        {
            scores = new List<Score>();

			// Hardcoded leaderboard
			scores.Add(new Score("Jean-Guy", 30, 913.43f));
			scores.Add(new Score("Marco", 20, 648.98f));
			scores.Add(new Score("Paulo", 20, 732.54f));
			scores.Add(new Score("Bob", 10, 745.78f));
			scores.Add(new Score("Ringo", 10, 867.43f));

            return;
        }  

        byte[] bytes = Convert.FromBase64String(data);  

        using (MemoryStream stream = new MemoryStream(bytes))
        {
            scores = (List<Score>)new BinaryFormatter().Deserialize(stream);
        }
    }

}
