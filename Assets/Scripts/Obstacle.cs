﻿
using UnityEngine;
using System.Collections;


/// <summary>
/// Script that will simulate debris from an explosion when the obstacle is hit.
/// </summary>

public class Obstacle : MonoBehaviour {

    [SerializeField] private bool hasDebris = true;
	[SerializeField] private bool explodes = false;
	public GameObject explosionEffect;
	[SerializeField] private float explosionForce;

	void Awake(){

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Destruction via torpedo, generates explosion force
	[RPC]
	public void impact(Vector3 impactPos)
    {
		audio.Play();
		if (hasDebris)
        {
            GetComponent<Collider>().enabled = false;
            rigidbody.isKinematic = false;
            foreach (Transform child in transform)
            {
                child.SetParent(null);
                child.rigidbody.useGravity = true;
                child.rigidbody.isKinematic = false;
                child.rigidbody.AddExplosionForce(explosionForce, impactPos, 1000.0f);

				foreach(Collider collider in child.GetComponents<Collider> ()) {
					collider.enabled = true;
				}
            }
        }

		if(explodes){
			GeneratExplosion();
		}
		Destroy(gameObject);
	}

	void GeneratExplosion(){
		GameObject explosion = (GameObject)GameObject.Instantiate(explosionEffect, transform.position, Quaternion.identity);
		explosion.transform.LookAt(Camera.main.transform.position);
	}
}
