﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MainMenuUIManager : MonoBehaviour {

    [SerializeField]
    private Text _connectedPlayerStatus;
    [SerializeField]
    private Transform _serverList;
    [SerializeField]
    private GameObject _serverItemButton;
    [SerializeField]
    private float _pollHostListTimeout = 5.0f;
    [SerializeField]
    private GameObject _joinMenu;
    [SerializeField]
    private GameObject _lobby;
    [SerializeField]
    private GameObject _lobbyBackButton;
	[SerializeField]
	private Toggle toggleAim;
    [SerializeField]
    private GameObject[] _scoreLines;

    private GameObject[] _instantiatedButtons = null;

	private bool introJoinPlayed = false;

	void Start(){
		toggleAim.isOn = ApplicationManager.instance.invertedAim;
	}

    void Update () {
        _connectedPlayerStatus.text = Network.connections.Length > 0 ? "A player is connected!" : "Waiting for another player...";
	}

    private void ActivateLobby()
    {
        _lobby.SetActive(true);
        _joinMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(_lobbyBackButton);
    }

    private IEnumerator RefreshServerListCoroutine()
    {
        for (int i = 0; _instantiatedButtons != null && i < _instantiatedButtons.Length; ++i)
            Destroy(_instantiatedButtons[i]);

        MasterServer.ClearHostList();
        NetworkManager.RequestServerList();

        float elapsedTime = 0.0f;
        HostData[] data;
        do
        {
            data = MasterServer.PollHostList();
            yield return null;
            elapsedTime += Time.deltaTime;
        } while (data.Length == 0 && elapsedTime <= _pollHostListTimeout);

        _instantiatedButtons = new GameObject[data.Length];

        int j = 0;
        foreach (HostData element in data)
        {
            _instantiatedButtons[j] = Instantiate(_serverItemButton) as GameObject;

            if (element.connectedPlayers >= 2)
            {
                _instantiatedButtons[j].SetActive(false);
            }
            else
            {
                _instantiatedButtons[j].SetActive(true);
                _instantiatedButtons[j].transform.SetParent(_serverList);
                _instantiatedButtons[j].GetComponentInChildren<Text>().text = element.gameName;
                HostData hostData = element;
                Button button = _instantiatedButtons[j].GetComponent<Button>();
                button.onClick.AddListener(() => NetworkManager.JoinServer(hostData));
                button.onClick.AddListener(() => ActivateLobby());
            }

            ++j;
        }

        yield return null;
    }


    public void RefreshServerList()
    {
		if(!introJoinPlayed){
			AudioManager.instance.playIntro();
			StartCoroutine(NextJoinSoundTimer());
		}
		StartCoroutine(RefreshServerListCoroutine());
    }

	private IEnumerator NextJoinSoundTimer(){
		introJoinPlayed = true;
		yield return new WaitForSeconds(3.0f);
		introJoinPlayed = false;
	}
    
    public void QuitApplication()
    {
        ApplicationManager.instance.ApplicationQuit();
    }

    public void HostGame()
    {
		AudioManager.instance.playIntro();
		ApplicationManager.instance.HostGame();
    }

    public void LoadGame()
    {
		ApplicationManager.instance.LoadGame();
    }

    public void Disconnect()
    {
        ApplicationManager.instance.Disconnect();
    }

    public void UpdateLeaderboardDisplay()
    {
        LeaderboardManager.LoadScores();
        for (int i = 0; i < LeaderboardManager.scores.Count && i < 10; ++i)
        {
            TimeSpan time = TimeSpan.FromSeconds(LeaderboardManager.scores[i].time);
            _scoreLines[i].transform.Find("Score+Partner/Time").GetComponent<Text>().text = string.Format("{0:D2}:{1:D2}:{2:D3}",
                time.Minutes,
                time.Seconds,
                time.Milliseconds);
            _scoreLines[i].transform.Find("Score+Partner/Score").GetComponent<Text>().text = LeaderboardManager.scores[i].points.ToString();
            _scoreLines[i].transform.Find("Score+Partner/Partner").GetComponent<Text>().text = LeaderboardManager.scores[i].partnerName;
            _scoreLines[i].SetActive(true);
        }
    }

}
