﻿using UnityEngine;
using System.Collections;

public class WaterSurface : MonoBehaviour {

    public static float currentSurfaceLevel { get; private set; }

    [SerializeField]
    private float surfaceLevel = 65.0f;

	void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Submarine"))
        {
            currentSurfaceLevel = surfaceLevel;
            collider.GetComponent<Submarine>().isNearSurface = true;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.CompareTag("Submarine"))
        {
            collider.GetComponent<Submarine>().isNearSurface = false;
        }
    }

}
